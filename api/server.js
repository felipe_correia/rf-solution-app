const express = require('express');
const compression = require('compression');
const bodyParser = require('body-parser');
const logger = require('morgan');
const errorHandler = require('errorhandler');
const dotenv = require('dotenv');
const mongoose = require('mongoose');
const cors = require('cors');
const path = require('path');

dotenv.load({
    path: '.env'
})

const app = express();
const server = require('http').Server(app);
var port = process.env.PORT || 3000;
app.use(compression());
app.use(logger('dev'));
app.use(cors({
    origin: '*',
    allowedHeaders: ['Content-Type', 'Authorization', 'X-Requested-With', 'Accept', 'Origin']
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

mongoose.connect(process.env.MONGODB_URI, { useMongoClient: true });
mongoose.connection.on('error', function () {
    console.log('Falha ao conectar com o Bando de Dados'),
        process.exit(1)
});

const router = require('./src/routes/api')(app);

server.listen(port, function () {
    console.log('Servidor está rodando na porta ' + port + " em modo "+ app.get('env'));
});

module.exports = app;