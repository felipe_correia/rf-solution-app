const mongoose = require('mongoose');

var contactSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Informe o nome']
    },
    email: {
        type: String,
        required: [true, 'Informe o e-mail']
    },
    data: {
        type: String,
        required: [true, 'Informe o data']
    },
    subject: {
        type: String,
        required: [true, 'Informe a assunto']
    },
    description: {
        type: String,
        required: [true, 'Informe a descrição']
    },
    opinion: {
        type: String
    },
    status: {
        type: Boolean
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('contact', contactSchema);