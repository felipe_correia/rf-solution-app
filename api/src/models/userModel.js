const mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Informe o nome do Usuário']
    },
    email: {
        type: String,
        required: [true, 'Informe o e-mail do Usuário']
    },
    address: {
        type: String,
        required: [true, 'Informe o endereço do Usuário']
    },
    password: {
        type: String,
        required: [true, 'Informe a senha do Usuário']
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('user', userSchema);