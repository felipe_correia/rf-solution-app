const mongoose = require('mongoose');
const contact = require('../models/ContactModel');
const serverUtil = require('../util/ServerUtil');

exports.registerContact = function (req, res) {
    var ContactObj = new contact({
        name: req.body.nome,
        email: req.body.email,
        data: req.body.data,
        subject: req.body.assunto,
        description: req.body.descricao,
        opinion: req.body.opnion,
        status: req.body.status
    });

    const msgreturn = "Informações salvas com sucesso!"
    return serverUtil.register(req, res, ContactObj, msgreturn);
}

exports.getAllContact = function (req, res) {
    return serverUtil.getAll(req, res, contact);
}

exports.getContactById = function (req, res) {
    const _id = req.params.id;
    return serverUtil.getById(req, res, contact, _id);
}

exports.deleteContactById = function (req, res) {
    const msgreturn = "Registro excluido com sucesso!"
    const _id = req.params.id;
    return serverUtil.deleteById(req, res, contact, _id, msgreturn);
}

exports.updateContact = function (req, res) {
    const _id = req.params.id;
    var ContactObj = {
        name: req.body.nome,
        email: req.body.email,
        data: req.body.data,
        subject: req.body.assunto,
        description: req.body.descricao,
        opnion: req.body.parecer,
        status: req.body.status
    };
    return serverUtil.update(req, res, contact, ContactObj, _id);
}


exports.updateOpnionContact = function (req, res) {
    const _id = req.params.id;
    var ContactObj = {
        opnion: req.body.parecer,
        status: true
    };

    return serverUtil.update(req, res, contact, ContactObj, _id);
}

exports.registerBaseContact = function (req, res) {
    contact.create({
        name: "Felipe",
        email: "felipe.contato@saq.com",
        data: "13/12/2018 as 18h30",
        subject: "Erro ao acessar o sistema",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since th",
        opinion: "",
        status: false
    }, {
        name: "Jorge",
        email: "jorge.contato@saq.com",
        data: "01/12/2018 as 12h00",
        subject: "Como criar o cadastro",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since th",
        opinion: "",
        status: false
    }, {
        name: "Jose",
        email: "jose.contato@saq.com",
        data: "01/11/2018 as 18h30",
        subject: "Reportar Usuário",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since th",
        opinion: "",
        status: false
    }, {
        name: "João",
        email: "joao.contato@saq.com",
        data: "18/10/2018 as 12h00",
        subject: "Gostaria de uma proposta",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since th",
        opinion: "",
        status: false
    }, function (err, contacts) {
        if (err) {
            console.log(err);
            return res.status(500).send({
                message: 'Erro ao tentar executar a ação no banco de dados'
            });
        }

        return res.status(200).send({
            message: 'Ação no banco de dados executada com Sucesso',
            obj: contacts
        })

    });

    // const msgreturn = "Informações salvas com sucesso!"
    // return serverUtil.register(req, res, ContactObj, msgreturn);
}