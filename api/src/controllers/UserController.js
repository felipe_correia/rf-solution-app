const mongoose = require('mongoose');
const user = require('../models/UserModel');
const serverUtil = require('../util/ServerUtil');

exports.registerUser = function (req, res) {
    var UserObj = new user({
        name: req.body.nome,
        email: req.body.email,
        address: req.body.endereco,
        password: req.body.senha
    });

    const msgreturn = "Informações salvas com sucesso!"
    return serverUtil.register(req, res, UserObj, msgreturn);
}

exports.getAllUser = function (req, res) {
    return serverUtil.getAll(req, res, user);
}

exports.getUserById = function (req, res) {
    const _id = req.params.id;
    return serverUtil.getById(req, res, user, _id);
}

exports.deleteUserById = function (req, res) {
    const msgreturn = "Registro excluido com sucesso!"
    const _id = req.params.id;
    return serverUtil.deleteById(req, res, user, _id, msgreturn);
}

exports.updateUser = function (req, res) {
    const _id = req.params.id;
    var UserObj = {
        name: req.body.nome,
        email: req.body.email,
        address: req.body.endereco,
        password: req.body.senha
    };
    return serverUtil.update(req, res, user, UserObj, _id);
}

exports.validateUser = function (req, res) {
    var UserObj = {
        email: req.query.email,
        password: req.query.senha
    };

    user.find(UserObj).exec(function (err, objReturn) {
        if (objReturn.length > 0) {
            return res.status(200).send({
                isvalid: true
            });
        }
        return res.status(200).send({
            isvalid: false,
            message: "usuário e/ou senha inválidos"
        });
    });
}