const express = require('express');
const userController = require('../controllers/UserController')
const contactController = require('../controllers/ContactController')

module.exports = function (app) {
    const route = express();
    // app.use('/api', function (req, res) {
    //     res.writeHead(200);
    //     res.end('API connect');
    // });

    // app.use('user/validate',
    //     route.get('/', userController.getAllUser));

    app.use('/api',
        //Usuários
        route.post('/user', userController.registerUser),
        route.get('/user', userController.getAllUser),
        route.get('/user/:id', userController.getUserById),
        route.delete('/user/:id', userController.deleteUserById),
        route.put('/user/:id', userController.updateUser),
        route.get('/userValidate', userController.validateUser),
        //Contato
        route.post('/contact', contactController.registerContact),
        route.get('/contact', contactController.getAllContact),
        route.get('/contact/:id', contactController.getContactById),
        route.delete('/contact/:id', contactController.deleteContactById),
        route.put('/contact/:id', contactController.updateContact),
        route.post('/contactBase', contactController.registerBaseContact),
        route.put('/contactSaveOpnion/:id', contactController.updateOpnionContact)
    );
    // app.use('/user/validate', userController.getAllUser);
}