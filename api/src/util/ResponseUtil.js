exports.validate = function (res, err, model, customMsg, customReturn) {
    if (err) {
        console.log(err);
        return res.status(500).send({
            message: customMsg || 'Erro ao tentar executar a ação no banco de dados'
        });
    }

    return res.status(200).send({
        message: customMsg || 'Ação no banco de dados executada com Sucesso',
        obj: model || customReturn
    })

}