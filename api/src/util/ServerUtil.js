const responseUtil = require('../util/ResponseUtil');

exports.register = function (req, res, obj, customMsgReturn, customReturn) {
    obj.save(function (err, objReturn) {
        return responseUtil.validate(res, err, objReturn, customMsgReturn, customReturn);
    });
}

exports.getAll = function (req, res, obj, customMsgReturn, customReturn) {
    var qry = obj.find();

    qry.exec(function (err, objReturn) {
        return responseUtil.validate(res, err, objReturn, customMsgReturn, customReturn);
    })
}

exports.getByObj = function (req, res, obj, filter, customMsgReturn, customReturn) {
    var qry = obj.find(filter);

    qry.exec(function (err, objReturn) {
        return responseUtil.validate(res, err, objReturn, customMsgReturn, customReturn);
    })
}

exports.getById = function (req, res, obj, _id, customMsgReturn, customReturn) {
    obj.findById(_id, function (err, objReturn) {
        return responseUtil.validate(res, err, objReturn, customMsgReturn, customReturn);
    })
}

exports.deleteById = function (req, res, obj, _id, customMsgReturn, customReturn) {
    obj.findById(_id, function (err, objReturn) {
        if (err) {
            return responseUtil.validate(res, err, objReturn);
        }
        obj.remove({
            _id: _id
        }, function (err, objReturn) {
            return responseUtil.validate(res, err, objReturn, customMsgReturn, customReturn);
        });
    });
}

exports.update = function (req, res, obj, body, _id, customMsgReturn, customReturn) {
    obj.findOneAndUpdate({
        _id: req.params.id
    }, body, function (err, objReturn) {
        return responseUtil.validate(res, err, objReturn, customMsgReturn, customReturn);
    });
}