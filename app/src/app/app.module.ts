import { ProfileConfigPage } from './../pages/profileConfig/profileConfig';
import { FeedPage } from './../pages/feed/feed';
import { ContactInfoPage } from '../pages/contactInfo/contactInfo';
import { ContactDetailPage } from '../pages/contactDetail/contactDetail';

import {  NotificationPage } from './../pages/notification/notification';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpModule } from '@angular/http';
import { BrMaskerModule } from 'brmasker-ionic-3';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { LoginPage } from '../pages/login/login';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Network } from '@ionic-native/network';
import { IonicStorageModule } from '@ionic/storage';
import { DatePipe } from '@angular/common'
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { File } from '@ionic-native/file';
import { SelectSearchableModule } from 'ionic-select-searchable';
import { AppState } from './app.global';
import { UsuarioProvider } from '../providers/usuario/usuario';
import { HttpClientModule } from '@angular/common/http';
import { ContatoProvider } from '../providers/contato/contato';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    ProfileConfigPage,
    FeedPage,
    ContactInfoPage,
    NotificationPage,
    ContactDetailPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, {
      backButtonText: '',
    }),
    HttpClientModule,
    HttpModule,
    SelectSearchableModule,
    BrMaskerModule,
    IonicStorageModule.forRoot(),


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    ProfileConfigPage,
    FeedPage,
    ContactInfoPage,
    NotificationPage,
    ContactDetailPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AppState,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ScreenOrientation,
    Network,
    DatePipe,
    UniqueDeviceID,
    File,
    SelectSearchableModule,
    UsuarioProvider,
    ContatoProvider

  ]
})
export class AppModule { }
