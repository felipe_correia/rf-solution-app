export class TruckerModel {
    name: string;
    email: string;
    CNPJ: string;
    CPF: string;
    birth: Date;
    cellphone: string;
    telephone: string;
    profile: string;
    subprofile: string;
    cep: string;
    // numero: number;
    // logradouro: string;
    // complemento: string;
    bairro: string;
    localidade: string;
    uf: string;
    // gender: string;
    // civilStatus: string;
    creationDate: any;
    typeOfCompany: string;
    corporateName: string;
    eventName: string;
    truckerModel: string;
    truckerBrand: string;


    constructor() {
        this.name = "";
        this.email = "";
        // this.CPF = "";
        // this.CNPJ = "";
        this.profile = "";
        this.subprofile = "";
        this.cep = "";
        // this.logradouro = "";
        // this.complemento = "";
        this.bairro = "";
        this.localidade = "";
        this.uf = "";
        // this.gender = "";
        // this.civilStatus = "";
        this.typeOfCompany = "";
        this.corporateName = "";
        this.eventName = "";
        this.truckerModel = "";
        this.truckerBrand = "";
    }

    public loadTrucker(response: any) {
        this.name = response.name;
        this.email = response.email;
        this.CPF = response.CPF;
        this.profile = response.profile;
        this.subprofile = response.subprofile;
        this.cep = response.cep;
        // this.logradouro = response.logradouro;
        // this.complemento = response.complemento;
        this.bairro = response.bairro;
        this.localidade = response.localidade;
        this.uf = response.uf;
        // this.gender = response.gender;
        // this.civilStatus = response.civilStatus;
        this.CNPJ = response.CNPJ;
        this.birth = response.birth;
        this.cellphone = response.cellphone;
        this.telephone = response.telephone;
        // this.numero = response.numero;
        this.creationDate = response.creationDate;
        this.typeOfCompany = response.typeOfCompany;
        this.corporateName = response.corporateName;
        this.eventName = response.eventName;
    }
}