import { CountModel } from './count';
import { TruckerModel } from './trucker';

export class TruckerList{
    key: string;
    trucker: TruckerModel[];
    count: CountModel[];

    constructor() {
        this.trucker = new Array<TruckerModel>();
        this.count = new Array<CountModel>();
    }
}