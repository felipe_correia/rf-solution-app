import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { environment as ENV } from '../../environments/environments';
/*

/*
  Generated class for the ContatoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ContatoProvider {

  constructor(public http: Http) {

  }
  salvar(obj) {
    return new Promise((resolve, reject) => {
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      this.http.post(ENV.BASE_URL + 'contact/', JSON.stringify(obj), { headers: header })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        })
    })
  }

  salvarParecer(obj, _id) {
    return new Promise((resolve, reject) => {
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      this.http.put(ENV.BASE_URL + '/contactSaveOpnion/' + _id, JSON.stringify(obj), { headers: header })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        })
    })
  }

  atualizar(obj, _id) {
    return new Promise((resolve, reject) => {
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      this.http.put(ENV.BASE_URL + '/contact/' + _id, JSON.stringify(obj), { headers: header })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        })
    })
  }

  listar() {
    return new Promise((resolve, reject) => {
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      this.http.get(ENV.BASE_URL + '/contact/', { headers: header })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        })
    })
  }

  obterPorId(_id) {
    return new Promise((resolve, reject) => {
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      this.http.get(ENV.BASE_URL + '/contact/' + _id, { headers: header })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        })
    })
  }

}
