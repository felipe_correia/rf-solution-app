import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { environment as ENV } from '../../environments/environments';
/*
/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {

  constructor(public http: Http) {

  }

  salvar(obj) {
    return new Promise((resolve, reject) => {
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      this.http.post(ENV.BASE_URL + '/user/', JSON.stringify(obj), { headers: header })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        })
    })
  }

  autenticar(obj: any) {
    return new Promise((resolve, reject) => {
      let header = new Headers();
      header.append('Content-Type', 'application/json');

      this.http.get(ENV.BASE_URL + '/userValidate', { headers: header, params: { email: obj.email, senha: obj.senha } })
        .subscribe(res => {
          let data = res.json();
          resolve(data);
        }, err => {
          reject(err);
        })
    })
  }

}
