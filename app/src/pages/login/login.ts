import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, NavParams, MenuController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { Network } from '@ionic-native/network';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {


  constructor(
    private network: Network,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private alertCtrl: AlertController,
    public navParams: NavParams,) {
     this.menuCtrl.enable(false, 'menu-material');
  }

  ngOnInit() {

  }

  Home() {
    this.navCtrl.push(HomePage);
  }

  OpenFacebook() {
    window.open('https://apps.facebook.com/', '_system', 'location=yes');
  };
}
