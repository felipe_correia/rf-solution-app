import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, NavParams, MenuController, LoadingController, ToastController } from 'ionic-angular';
import { ProfileConfigPage } from '../profileConfig/profileConfig';
import { FeedPage } from '../feed/feed';
import { File } from '@ionic-native/file';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public loadingMsg;
  public formUsuario: FormGroup;

  constructor(
    public file: File,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private alertCtrl: AlertController,

    public UsuarioService: UsuarioProvider,
    public loading: LoadingController,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public fb: FormBuilder) {
    this.formUsuario = fb.group({
      email: [null, [Validators.required]],
      senha: [null, [Validators.required]]
    });
    this.menuCtrl.enable(false, 'menu-material');

  }

  ngOnInit() {

  }

  Login() {
    this.showLoader();
    this.UsuarioService.autenticar(this.formUsuario.value)
      .then((res: any) => {
        try {

          this.loadingMsg.dismiss();
          if (res.isvalid) {
            this.navCtrl.push(FeedPage);
          } else {
            this.showMessage(res.message, 2500);
          }
        } catch (err) {
          console.log(err);
        }
      })
      .catch((err) => {
        console.log(err)
      });
  }
  showMessage(msg: string, time?: number) {
    let message = this.toastCtrl.create({
      message: msg,
      duration: time,
      showCloseButton: true,
      closeButtonText: 'OK'
    });
    message.present();
  }
  showLoader() {
    this.loadingMsg = this.loading.create({
      content: "Aguarde ..."
    });
  }
  NewProfile() {
    this.navCtrl.push(ProfileConfigPage);
  }
  OpenFacebook() {
    window.open('https://apps.facebook.com/', '_system', 'location=yes');
  };
}
