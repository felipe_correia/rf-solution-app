import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, AlertController, MenuController, LoadingController } from 'ionic-angular';
import { ContactDetailPage } from '../contactDetail/contactDetail';
import { FormBuilder } from '@angular/forms';
import { ContatoProvider } from '../../providers/contato/contato';
declare var google;

/**
 * Generated class for the contactInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contactInfo',
  templateUrl: 'contactInfo.html',
})
export class ContactInfoPage {

  public cardList: any[];
  public loadingMsg;

  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public navParams: NavParams,
    public loading: LoadingController,
    public modalCtrl: ModalController,
    private alertCtrl: AlertController,
    public ContatoService: ContatoProvider,
    public fb: FormBuilder) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
    this.carregarLista();
  }

  details(id) {
    let modal = this.modalCtrl.create(ContactDetailPage, {
      valueId: id
    });

    modal.present();

    modal.onDidDismiss(data => {
      this.carregarLista();
    });
  }
  carregarLista() {
    this.ContatoService.listar()
      .then((res: any) => {
        try {
          this.cardList = res.obj;
        } catch (err) {
          console.log(err);
        }
      })
      .catch((err) => {
        console.log(err)
      });
  }
  showLoader() {
    this.loadingMsg = this.loading.create({
      content: "Aguarde ..."
    });
  }
}
