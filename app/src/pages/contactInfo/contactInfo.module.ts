import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular/umd';
import { ContactInfoPage } from './contactInfo';

@NgModule({
  declarations: [
    ContactInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactInfoPage)
  ],
})
export class contactInfoPageModule {}

