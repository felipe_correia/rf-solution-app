import { CountModel } from './../../Models/count';
import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, NavParams, MenuController } from 'ionic-angular';
import { NotificationPage } from '../notification/notification';
import { ContactInfoPage } from '../contactInfo/contactInfo';
import { TruckerList } from '../../Models/trucker-list';
import { TruckerModel } from '../../Models/trucker';
import { File } from '@ionic-native/file';
import { EmailModel } from '../../Models/email';
import { ContatoProvider } from '../../providers/contato/contato';

@Component({
  selector: 'page-feed',
  templateUrl: 'feed.html'
})
export class FeedPage {

  public countAguardando;
  public countHoje;
  public countValidado;


  constructor(
    public file: File,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    public ContatoService: ContatoProvider,
    public navParams: NavParams) {
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
    this.carregarLista();
  }


  Notification() {
    this.navCtrl.push(NotificationPage);
  }

  ContactInfo() {
    this.navCtrl.push(ContactInfoPage);
  }

  carregarLista() {
    this.ContatoService.listar()
      .then((res: any) => {
        try {
          var result = res.obj.filter(obj => {
            return obj.status === false
          })

          console.log(result);
          this.countAguardando = result.length;
          this.countHoje = result.length;
          this.countValidado = res.obj.length - result.length;
        } catch (err) {
          console.log(err);
        }
      })
      .catch((err) => {
        console.log(err)
      });
  }


}
