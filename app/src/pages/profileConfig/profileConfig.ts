
import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, NavParams, MenuController, LoadingController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { UsuarioProvider } from '../../providers/usuario/usuario';
import { HomePage } from '../home/home';
@Component({
  selector: 'page-profile-config',
  templateUrl: 'profileConfig.html'
})
export class ProfileConfigPage {
  public formUsuario: FormGroup;
  public loadingMsg;
  constructor(
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    public UsuarioService: UsuarioProvider,
    public loading: LoadingController,
    public toastCtrl: ToastController,
    public fb: FormBuilder) {
    this.formUsuario = fb.group({
      email: [null, [Validators.required]],
      senha: [null, [Validators.required]],
      nome: [null, [Validators.required]],
      endereco: [null, [Validators.required]]
    });
    this.menuCtrl.enable(false, 'menu-material');

  }

  salvarUsuario() {
    this.showLoader();
    this.UsuarioService.salvar(this.formUsuario.value)
      .then((res: any) => {
        try {
          this.loadingMsg.dismiss();
          this.showMessage(res.message, 2500);
          this.navCtrl.push(HomePage);
        } catch (err) {
          console.log(err);
        }
      })
      .catch((err) => {
        console.log(err)
      });
  }

  showLoader() {
    this.loadingMsg = this.loading.create({
      content: "Aguarde ..."
    });
  }

  showMessage(msg: string, time?: number) {
    let message = this.toastCtrl.create({
      message: msg,
      duration: time,
      showCloseButton: true,
      closeButtonText: 'OK'
    });
    message.present();
  }
  ngOnInit() {

  }


}
