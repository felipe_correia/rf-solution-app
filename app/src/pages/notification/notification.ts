import { CountModel } from '../../Models/count';
import { Component, ChangeDetectorRef } from '@angular/core';
import { NavController, AlertController, NavParams, MenuController } from 'ionic-angular';
import { Network } from '@ionic-native/network';
import { TruckerList } from '../../Models/trucker-list';
import { TruckerModel } from '../../Models/trucker';
import { File } from '@ionic-native/file';
import * as XLSX from 'xlsx';
import { EmailModel } from '../../Models/email';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';

@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html'
})
export class NotificationPage {
  status: boolean;
  storage: boolean;
  truckers: TruckerList[];
  trucker: TruckerModel = new TruckerModel();
  count: CountModel = new CountModel();
  statusInternet: boolean;
  totalCad: any;
  totalRegisterTrucker: any[] = [];
  totalRegisterCount: any[] = [];
  data: any;
  allTrucker: any;
  data2: any;
  emailModel: EmailModel = new EmailModel();
  uniqueid: string;
  event: string;


  constructor(
    public file: File,
    private uniqueDeviceID: UniqueDeviceID,
    private network: Network,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private alertCtrl: AlertController,
    public navParams: NavParams,
    private ref: ChangeDetectorRef) {
      this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {

  }

}
