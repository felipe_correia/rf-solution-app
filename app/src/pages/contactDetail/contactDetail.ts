import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, ModalController, ViewController, NavParams, AlertController, MenuController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ContatoProvider } from '../../providers/contato/contato';
declare var google;

/**
 * Generated class for the contactDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contactDetail',
  templateUrl: 'contactDetail.html',
})
export class ContactDetailPage {

  public valueId: string;
  public Data: string;
  public Email: string;
  public Assunto: string;
  public Descricao: string;
  public Parecer: string;

  public formContato: FormGroup;

  constructor(
    public viewCtrl: ViewController,
    public menuCtrl: MenuController,
    params: NavParams,
    public ContatoService: ContatoProvider,
    public fb: FormBuilder) {
    this.valueId = params.get('valueId');

    this.formContato = fb.group({
      parecer: [null, [Validators.required]],
      email: [null, [Validators.required]],
      data: [null, [Validators.required]],
      assunto: [null, [Validators.required]],
      descricao: [null, [Validators.required]],
    });
    this.menuCtrl.enable(true, 'menu-material');
  }

  ngOnInit() {
    this.ContatoService.obterPorId(this.valueId)
      .then((res: any) => {
        try {

          this.formContato = this.fb.group({
            parecer: [null, [Validators.required]],
            email: [res.obj.email, [Validators.required]],
            data: [res.obj.data, [Validators.required]],
            assunto: [res.obj.subject, [Validators.required]],
            descricao: [res.obj.description, [Validators.required]]
          });

          // this.formContato.setValue({
          //   data: res.obj.data,
          //   email: res.obj.email,
          //   assunto: res.obj.subject,
          //   descricao: res.obj.description
          // });
        } catch (err) {
          console.log(err);
        }
      })
      .catch((err) => {
        console.log(err)
      });
  }

  save() {
    this.ContatoService.salvarParecer(this.formContato.value, this.valueId)
      .then((res: any) => {
        try {
          this.viewCtrl.dismiss();
        } catch (err) {
          console.log(err);
        }
      })
      .catch((err) => {
        console.log(err)
      });
  }

  cancel() {
    this.viewCtrl.dismiss();
  }


}
